package utils;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.UserDao;

public class ValidationUtil {

	public static boolean checkDuplicate(String loginId) {
		Connection connection = null;
		try {
			connection = getConnection();

			boolean result = new UserDao().isDuplicateUser(connection, loginId);

			commit(connection);

			return result;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public static boolean CharLimit(int from, int to, String st) {
		return from <= st.length() && st.length() <= to;
	}

	public static boolean loginIdCheck(int from, int to, String st) {
		return from <= st.length() && st.length() <= to &&
				st.matches("^[0-9a-zA-Z]+$");
	}

	public static boolean passCheck(int from, int to, String st) {
		return from <= st.length() && st.length() <= to &&
				st.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$");
	}

}
