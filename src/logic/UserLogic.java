package logic;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.MergeUser;
import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserLogic {
	private static final int LIMIT_NUM = 1000;

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			//パスワードを暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<User> getUsers(){
		Connection connection = null;
		try {
			connection = getConnection();

			List<User> Users = new UserDao().getUserList(connection, LIMIT_NUM);

			commit(connection);

			return Users;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}

	}

	public List<MergeUser> getMergeUsers(){
		Connection connection = null;
		try {
			connection = getConnection();

			List<MergeUser> MergeUsers = new UserDao().getMergeUserList(connection, LIMIT_NUM);

			commit(connection);

			return MergeUsers;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}

	}



	public void update(User editUser) {
		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isEmpty(editUser.getPassword())) {
				String encPassword = CipherUtil.encrypt(editUser.getPassword());
				editUser.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, editUser);

			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}

	}

	public void revivalOrKill(int editUserId, int flg) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.revivalOrKill(connection, editUserId, flg);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public User getEditUser(String editUserId) {
		Connection connection = null;
		try {
			connection = getConnection();

			User user = new UserDao().getEditUser(connection, editUserId);

			commit(connection);

			return user;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
