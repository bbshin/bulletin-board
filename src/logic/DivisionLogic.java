package logic;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Division;
import dao.DivisionDao;

public class DivisionLogic {

	public List<Division> getDivisions() {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Division> divisions = new DivisionDao().getDivisionList(connection);

			commit(connection);

			return divisions;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

}
