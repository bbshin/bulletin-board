package logic;
import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.MergeComment;
import dao.CommentDao;

public class CommentLogic {
	private static final int LIMIT_NUM = 1000;

	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<MergeComment> getComments(){
		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			List<MergeComment> com = commentDao.getMergeCommentList(connection, LIMIT_NUM);

			commit(connection);

			return com;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void deleteComment(int commentId) {
		Connection connection = null;
		try {
			connection = getConnection();

			new CommentDao().delete(connection, commentId);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
