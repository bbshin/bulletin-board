package logic;
import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.MergePost;
import beans.Post;
import dao.MergePostDao;
import dao.PostDao;

public class PostLogic {
	private static final int LIMIT_NUM = 1000;

	//DBへ投稿を登録するメソッド
	public void register(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	//DBから投稿を取得するメソッド
	public List<MergePost> getMergePosts(String word, String start_at, String end_at){
		Connection connection = null;
		try {
			connection = getConnection();

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String startDate = "";
			String endDate = "";

			if(!(StringUtils.isEmpty(start_at)) && !(StringUtils.isEmpty(end_at))) {
				startDate = start_at + " 00:00;00";
				endDate = end_at + " 23:59;59";
			}else if(StringUtils.isEmpty(start_at) && !(StringUtils.isEmpty(end_at))) {
				startDate = "2000-01-01 00:00;00";
				endDate = end_at + " 23:59;59";
			}else if(!(StringUtils.isEmpty(start_at)) && StringUtils.isEmpty(end_at)) {
				startDate = start_at + " 00:00;00";
				endDate = dateFormat.format(date);
			}else {
				startDate = "2000-01-01 00:00;00";
				endDate = dateFormat.format(date);
			}

			MergePostDao mergePostDao = new MergePostDao();
			List<MergePost> pos = mergePostDao.getPostList(connection, word, startDate, endDate, LIMIT_NUM);

			commit(connection);

			return pos;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	//DBから投稿を削除するメソッド
	public void deletePost(int postId) {
		Connection connection = null;
		try {
			connection = getConnection();

			new PostDao().delete(connection, postId);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
