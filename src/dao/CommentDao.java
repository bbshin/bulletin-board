package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.MergeComment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments (");
			sql.append("user_id");
			sql.append(", post_id");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUser_id());
			ps.setInt(2, comment.getPost_id());
			ps.setString(3, comment.getText());

			ps.executeUpdate();
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int commentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId);

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public List<MergeComment> getMergeCommentList(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id,");
			sql.append("comments.user_id as userId,");
			sql.append("comments.post_id as postId,");
			sql.append("comments.text as text,");
			sql.append("users.name as name,");
			sql.append("comments.updated_date as updatedDate, ");
			sql.append("comments.updated_date as createdDate ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY updatedDate ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<MergeComment> com = toMergeCommentList(rs);
			return com;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<MergeComment> toMergeCommentList(ResultSet rs) throws SQLException{
		List<MergeComment> com = new ArrayList<>();
		try {
			while(rs.next()) {
				MergeComment mc = new MergeComment();
				mc.setId(rs.getInt("id"));
				mc.setUserId(rs.getInt("userId"));
				mc.setPostId(rs.getInt("postId"));
				mc.setText(rs.getString("text"));
				mc.setName(rs.getString("name"));
				mc.setUpdatedDate(rs.getTimestamp("updatedDate"));
				mc.setCreatedDate(rs.getTimestamp("createdDate"));

				com.add(mc);
			}
			return com;
		}finally {
			close(rs);
		}
	}
}
