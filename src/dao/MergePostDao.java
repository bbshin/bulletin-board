package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.MergePost;
import exception.SQLRuntimeException;

public class MergePostDao {

	public List<MergePost> getPostList(Connection connection, String word, String date1, String date2, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.user_id as userId, ");
			sql.append("posts.subject as subject, ");
			sql.append("posts.text as text, ");
			sql.append("posts.category as category, ");
			sql.append("users.name as name, ");
			sql.append("posts.updated_date as updatedDate, ");
			sql.append("posts.created_date as createdDate ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE posts.created_date ");
			sql.append("BETWEEN ? ");
			sql.append("AND ? ");
			if(!StringUtils.isEmpty(word)) {
				sql.append("AND posts.category LIKE ? ");
			}
			sql.append("ORDER BY updatedDate DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, date1);
            ps.setString(2, date2);
            if(!StringUtils.isEmpty(word)) {
            	String likeWord = "%"+word+"%";
            	ps.setString(3, likeWord);
            }

			ResultSet rs = ps.executeQuery();

			List<MergePost> pos = toMergePostList(rs);
			return pos;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<MergePost> toMergePostList(ResultSet rs) throws SQLException{
		List<MergePost> pos = new ArrayList<>();
		try {
			while(rs.next()) {
				MergePost mergePost = new MergePost();
				mergePost.setId(rs.getInt("id"));
				mergePost.setUserId(rs.getInt("userId"));
				mergePost.setSubject(rs.getString("subject"));
//				String insert_str = (rs.getString("text")).replaceAll("(\r\n|\r|\n)", "\n");
				mergePost.setText(rs.getString("text"));
				mergePost.setCategory(rs.getString("category"));
				mergePost.setName(rs.getString("name"));
				mergePost.setUpdatedDate(rs.getTimestamp("updatedDate"));
				mergePost.setCreatedDate(rs.getTimestamp("createdDate"));

				pos.add(mergePost);
			}
			return pos;
		}finally {
			close(rs);
		}
	}

}
