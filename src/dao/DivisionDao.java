package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Division;
import exception.SQLRuntimeException;

public class DivisionDao {

	public List<Division> getDivisionList(Connection connection){
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id, ");
			sql.append("name ");
			sql.append("FROM ");
			sql.append("divisions");

			ps = connection.prepareStatement(sql.toString());

			rs = ps.executeQuery();

			List<Division> divisions = toDivisionList(rs);

			return divisions;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<Division> toDivisionList(ResultSet rs) throws SQLException{

		List<Division> divisions = new ArrayList<Division>();
		try {
			while(rs.next()) {
				Division divisoin = new Division();
				divisoin.setId(rs.getInt("id"));
				divisoin.setName(rs.getString("name"));

				divisions.add(divisoin);
			}
			return divisions;
		}finally {
			close(rs);
		}
	}

}
