package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.MergeUser;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	//ユーザー登録
	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", division_id");
			sql.append(", is_alive");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDivisionId());
            ps.setInt(6, 1);

            ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", division_id = ?");
			if(!StringUtils.isEmpty(user.getPassword())) {
				sql.append(", password = ?");
			}
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps=connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getDivisionId());
			if(!StringUtils.isEmpty(user.getPassword())) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			}else {
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public List<User> getUserList(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id ");
			sql.append("FROM users ");
			sql.append("limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<User> users = toIdUser(rs);
			return users;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<User> toIdUser(ResultSet rs) throws SQLException{
		List<User> users = new ArrayList<User>();
		try {
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				users.add(user);
			}
			return users;
		}finally {
			close(rs);
		}
	}


	public List<MergeUser> getMergeUserList(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id,");
			sql.append("users.login_id as loginId,");
			sql.append("users.name as name,");
			sql.append("users.branch_id as branchId,");
			sql.append("branches.name as branchName,");
			sql.append("users.division_id as divisionId,");
			sql.append("divisions.name as divisionName,");
			sql.append("users.is_alive as isAlive,");
			sql.append("users.created_date as createdDate,");
			sql.append("users.updated_date as updatedDate ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN divisions ");
			sql.append("ON users.division_id = divisions.id ");
			sql.append("ORDER BY updatedDate DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<MergeUser> MergeUsers = toMergeUserList(rs);

			return MergeUsers;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	//ログイン時にユーザー情報を取得
	public User getUser(Connection connection, String loginId, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);	//rsの中身をbeansに詰めてListに格納

			if(userList.isEmpty()) {
				return null;
			}else if(2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			}else {
				return userList.get(0);
			}

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}



	public void revivalOrKill(Connection connection, int editUserId, int flg) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_alive = ?");
			sql.append(" WHERE id = ?");


			ps=connection.prepareStatement(sql.toString());

			ps.setInt(1, flg);
			ps.setInt(2, editUserId);

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public User getEditUser(Connection connection, String editUserId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, editUserId);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);
    		if(userList.isEmpty()) {
    			return null;
    		}else if(2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		}else {
    			return userList.get(0);
    		}
    	}catch(SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}
	}

	public boolean isDuplicateUser(Connection connection, String loginId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);
    		if(userList.isEmpty()) {
    			return false;
    		}else {
    			return true;
    		}
    	}catch(SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException{

		List<User> ret = new ArrayList<User>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branchId = rs.getInt("branch_id");
				int divisionId = rs.getInt("division_id");
				int isAlive = rs.getInt("is_alive");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setPassword(password);
				user.setBranchId(branchId);
				user.setDivisionId(divisionId);
				user.setIsAlive(isAlive);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(0, user);
			}
			return ret;
		}finally {
			close(rs);
		}
	}

	private List<MergeUser> toMergeUserList(ResultSet rs) throws SQLException{

		List<MergeUser> mul = new ArrayList<>();
		try {
			while(rs.next()) {
				MergeUser mu = new MergeUser();
				mu.setId(rs.getInt("id"));
				mu.setLoginId(rs.getString("loginId"));
				mu.setName(rs.getString("name"));
				mu.setBranchId(rs.getInt("branchId"));
				mu.setBranchName(rs.getString("branchName"));
				mu.setDivisionId( rs.getInt("divisionId"));
				mu.setDivisionName(rs.getString("divisionName"));
				mu.setIsAlive(rs.getInt("isAlive"));
				mu.setCreatedDate(rs.getTimestamp("createdDate"));
				mu.setUpdatedDate(rs.getTimestamp("updatedDate"));

				mul.add(mu);
			}
			return mul;

		}finally {
			close(rs);
		}
	}
}
