package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Division;
import beans.MergeUser;
import logic.BranchLogic;
import logic.DivisionLogic;
import logic.UserLogic;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<MergeUser> users = new UserLogic().getMergeUsers();
		session.setAttribute("users", users);

		List<Branch> branches = new BranchLogic().getBranches();
		session.setAttribute("branches", branches);

		List<Division> divisions = new DivisionLogic().getDivisions();
		session.setAttribute("divisions", divisions);


		request.getRequestDispatcher("admin.jsp").forward(request, response);
	}

}
