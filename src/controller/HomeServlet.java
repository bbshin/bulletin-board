package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.MergeComment;
import beans.MergePost;
import logic.CommentLogic;
import logic.PostLogic;




@WebServlet("/index.jsp")
public class HomeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String word = request.getParameter("word");
		String start_at = request.getParameter("startDate");
		String end_at = request.getParameter("endDate");

		List<MergePost> posts = new PostLogic().getMergePosts(word, start_at, end_at);
		request.setAttribute("PostList", posts);

		List<MergeComment> comments = new CommentLogic().getComments();
		request.setAttribute("comments", comments);

		request.setAttribute("word", word);
		request.setAttribute("startDate", start_at);
		request.setAttribute("endDate", end_at);

		request.getRequestDispatcher("home.jsp").forward(request, response);

	}
}
