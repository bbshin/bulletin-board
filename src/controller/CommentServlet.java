package controller;

import static utils.ValidationUtil.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import logic.CommentLogic;


@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		String com = request.getParameter("comment");

		if(isValid(com, messages)) {
			User user = (User)session.getAttribute("loginUser");

			Comment comment = new Comment();
			//紐付け
			comment.setUser_id(user.getId());
			comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));
			//
			comment.setText(com);

			new CommentLogic().register(comment);

			response.sendRedirect("./");
		}else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(String comment, List<String> messages) {
		String checkCom = comment.replace("\r\n", "");
		if(StringUtils.isBlank(checkCom)) {
			messages.add("コメントを入力してください");
		}

		if(!CharLimit(0, 500, checkCom)) {
			messages.add("コメントは500文字以下で入力してください");
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

}
