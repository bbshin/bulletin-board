package controller;

import static utils.ValidationUtil.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import logic.PostLogic;

@WebServlet("/post")
public class PostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("post.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		String category = request.getParameter("category");
		String subject = request.getParameter("subject");
		String text = request.getParameter("text");

		if(isValid(category, subject, text, messages)) {
			User user = (User) session.getAttribute("loginUser");
			Post post = new Post();
			post.setCategory(category);
			post.setSubject(subject);
			post.setText(text);
			//ユーザーと投稿を紐付け
			post.setUserId(user.getId());

			new PostLogic().register(post);

			response.sendRedirect("./");
		}else {
			Post retainPost = setRetainPost(category, subject, text);
			request.setAttribute("retainPost", retainPost);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("post.jsp").forward(request, response);
		}
	}

	private boolean isValid(String category, String subject, String text, List<String> messages) {

		if(StringUtils.isBlank(category)) {
			messages.add("カテゴリーを入力してください");
		}

		if(!CharLimit(0, 10, category)) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}

		if(StringUtils.isBlank(subject)) {
			messages.add("件名を入力してください");
		}

		if(!CharLimit(0, 30, subject)) {
			messages.add("件名は30文字以下で入力してください");
		}

		String checkText = text.replace("\r\n", "");
		if(StringUtils.isBlank(checkText)) {
			messages.add("投稿を入力してください");
		}

		if(!CharLimit(0, 1000, checkText)) {
			messages.add("投稿は1000文字以下で入力してください");
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

	private Post setRetainPost(String category, String subject, String text) {
		Post retainPost = new Post();
		retainPost.setCategory(category);
		retainPost.setSubject(subject);
		retainPost.setText(text);
		return retainPost;
	}

}
