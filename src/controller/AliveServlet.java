package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.UserLogic;


@WebServlet("/alive")
public class AliveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int editUserId = Integer.parseInt(request.getParameter("editUserId"));
		int flg = Integer.parseInt(request.getParameter("isAlive"));

		if(flg == 0) {
			flg=1;
		}else {
			flg=0;
		}

		new UserLogic().revivalOrKill(editUserId, flg);

		response.sendRedirect("admin");
	}

}
