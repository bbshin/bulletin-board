package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import logic.LoginLogic;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("login.jsp").forward(request, response);

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		request.setAttribute("retainLoginId", loginId);

		if(StringUtils.isBlank(loginId) || StringUtils.isBlank(password)) {
			String error = "ログインIDまたはパスワードが入力されていません";
			request.setAttribute("errorMessages", error);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}

		LoginLogic loginLogic = new LoginLogic();
		User user = loginLogic.login(loginId, password);

		if(user != null && user.getIsAlive() == 1) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		}else {
			String error = "ログインIDまたはパスワードが誤っています";
			request.setAttribute("errorMessages", error);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}

	}

}
