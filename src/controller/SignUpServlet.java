package controller;

import static utils.ValidationUtil.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Division;
import beans.User;
import logic.BranchLogic;
import logic.DivisionLogic;
import logic.UserLogic;


@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Branch> branches = new BranchLogic().getBranches();
		request.setAttribute("branches", branches);

		List<Division> divisions = new DivisionLogic().getDivisions();
		request.setAttribute("divisions", divisions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String pass = request.getParameter("password");
		String rePass = request.getParameter("rePassword");
		int branchId = (Integer.parseInt(request.getParameter("branchId")));
		int divisionId = (Integer.parseInt(request.getParameter("divisionId")));

		if(isValid(loginId, name, pass, rePass, branchId, divisionId, messages)) {
			User user = new User();
				user.setLoginId(loginId);
				user.setName(name);
				user.setPassword(pass);
				user.setBranchId(branchId);
				user.setDivisionId(divisionId);

			new UserLogic().register(user);
			response.sendRedirect("admin");
			return;
		}else {
			User retainUser = setRetainUser(loginId, name, branchId, divisionId);
			request.setAttribute("retainUser", retainUser);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(String loginId, String name, String pass, String rePass, int branchId, int divisionId, List<String> messages) {
		if(StringUtils.isBlank(loginId)){
			messages.add("ログインIDを入力してください");
		}else if(!loginIdCheck(6, 20, loginId)) {
			messages.add("ログインIDは6文字以上20文字以下の半角英数字で入力してください");
		}

		if(checkDuplicate(loginId)) {
			messages.add("そのログインIDは既に使用されています");
		}

		if(StringUtils.isBlank(name)) {
			messages.add("名称を入力してください");
		}

		if(!CharLimit(0, 10, name)) {
			messages.add("名称は10文字以下で入力してください");
		}

		if(StringUtils.isBlank(pass)) {
			messages.add("パスワードを入力してください");
		}else if(!passCheck(6, 20, pass)) {
			messages.add("パスワードは6文字以上20文字以下の半角英数字で入力してください");
		}

		if(!pass.equals(rePass)) {
			messages.add("パスワードが確認用パスワードと一致していません");
		}

		if(branchId == 1) {
			if(divisionId > 2) {
				messages.add("本社にその役職は存在しません");
			}
		}else {
			if(divisionId < 3) {
				messages.add("支店にその役職は存在しません");
			}
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}

	}

	private User setRetainUser(String loginId, String name, int branchId, int divisionId) {
		User retainUser = new User();
		retainUser.setLoginId(loginId);
		retainUser.setName(name);
		retainUser.setBranchId(branchId);
		retainUser.setDivisionId(divisionId);
		return retainUser;
	}

}
