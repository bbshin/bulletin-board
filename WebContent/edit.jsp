<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/reset.css" rel="stylesheet" type="text/css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<title>ユーザー編集画面</title>
</head>
<body>
	<header>
		<div id="header_inner">
			<h1>Bulletin board</h1>
			<p><i class="far fa-user-circle"></i> <c:out value="${loginUser.name}" />さん</p>
		</div>
		<nav>
			<ul>
				<li><a href="./"><i class="fas fa-home"></i>ホーム</a></li>
				<li><a href="post">新規投稿</a></li>
				<c:if test = "${loginUser.branchId == 1 && loginUser.divisionId == 1}">
					<li><a href="admin">ユーザー管理</a></li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div id="wrapper">
		<h2>ユーザー編集画面</h2>
		<div id="errorMessages">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
		<div id="edit_form" class="form">
			<form action="edit" method="post">
				<input name="id" value="${loginUser.id}" type="hidden" />
				<div class="item">
					<label for="loginId">ログインID</label>
					<input type="text" name="loginId" id="loginId" value="${retainEditUser.loginId}">
				</div>
				<div class="item">
					<label for="name">名称</label>
					<input type="text" name="name" id="name" value="${retainEditUser.name}">
				</div>
				<div class="item">
					<label for="pass">パスワード</label>
					<input type="password" name="password" id="pass">
				</div>
				<div class="item">
					<label for="repass">パスワード確認</label>
					<input type="password" name="rePassword" id="repass">
				</div>
				<c:choose>
					<c:when test="${loginUser.id == retainEditUser.id && loginUser.branchId == 1 && loginUser.divisionId == 1}">
						<div class="item">
							<label for="branch">支店</label>
							<select name="branchId" id="branch">
								<option value=1 selected="selected">本社</option>
							</select>
						</div>
						<div class="item">
							<label for="division">部署・役職</label>
							<select name="divisionId" id="division">
								<option value=1 selected="selected">人事総務部</option>
							</select>
						</div>
					</c:when>
					<c:otherwise>
						<div class="item">
							<label for="branch">支店</label>
							<select name="branchId" id="branch">
								<c:forEach items="${branches}" var="branch">
									<option value="${branch.id}" <c:if test="${retainEditUser.branchId == branch.id }"> selected="selected"</c:if>>
										<c:out value="${branch.name}" />
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="item">
							<label for="division">部署・役職</label>
							<select name="divisionId" id="division">
								<c:forEach items="${divisions}" var="division">
									<option value="${division.id}" <c:if test="${retainEditUser.divisionId == division.id }"> selected="selected"</c:if>>
										<c:out value="${division.name}" />
									</option>
								</c:forEach>
							</select>
						</div>
					</c:otherwise>
				</c:choose>
				<div class="submit_btn">
					<input type="submit" value="送信">
				</div>
			</form>
		</div>
	</div>
	<footer>Copyright(c) Matsunaga Shinya</footer>
</body>
</html>