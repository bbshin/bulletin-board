<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/reset.css" rel="stylesheet" type="text/css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<title>ユーザー登録画面</title>
</head>
<body>
	<header>
		<div id="header_inner">
			<h1>Bulletin board</h1>
			<p><i class="far fa-user-circle"></i> <c:out value="${loginUser.name}" />さん</p>
		</div>
		<nav class="admin">
			<ul>
				<li><a href="./"><i class="fas fa-home"></i>ホーム</a></li>
				<li><a href="post">新規投稿</a></li>
				<c:if test = "${loginUser.branchId == 1 && loginUser.divisionId == 1}">
					<li><a href="admin">ユーザー管理</a></li>
				</c:if>
				<c:if test = "${loginUser.branchId == 1 && loginUser.divisionId == 1}">
					<li><a href="signup">ユーザー登録</a></li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div id="wrapper">
		<h2>ユーザー登録画面</h2>
		<div id="errorMessages">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>

		<div id="signup_form" class="form">
			<form action="signup" method="post">
				<div class="item">
					<label for="loginId">ログインID</label>
					<input type="text" name="loginId" id="loginId" value="${retainUser.loginId}">
				</div>
				<div class="item">
					<label for="name">名称</label>
					<input type="text" name="name" id="name" value="${retainUser.name}">
				</div>
				<div class="item">
					<label for="pass">パスワード</label>
					<input type="password" name="password" id="pass">
				</div>
				<div class="item">
					<label for="repass">パスワード確認</label>
					<input type="password" name="rePassword" id="repass">
				</div>

				<div class="item">
					<label for="branch">支店</label>
					<select name="branchId" id="branch">
						<c:forEach items="${branches}" var="branch">
							<c:choose>
								<c:when test="${retainUser.branchId == branch.id }">
									<option value="${branch.id}" selected="selected"><c:out value="${branch.name}" /></option>
								</c:when>
								<c:otherwise>
									<option value="${branch.id}"><c:out value="${branch.name}" /></option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>

				<div class="item">
					<label for="division">部署・役職</label>
					<select name="divisionId" id="division">
						<c:forEach items="${divisions}" var="division">
							<c:choose>
								<c:when test="${retainUser.divisionId == division.id }">
									<option value="${division.id}" selected="selected"><c:out value="${division.name}" /></option>
								</c:when>
								<c:otherwise>
									<option value="${division.id}"><c:out value="${division.name}" /></option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="submit_btn">
					<input type="submit" value="登録">
				</div>
			</form>
		</div>
	</div>
	<footer>Copyright(c) Matsunaga Shinya</footer>
</body>
</html>