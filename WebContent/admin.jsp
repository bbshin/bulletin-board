<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/reset.css" rel="stylesheet" type="text/css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<title>ユーザー管理画面</title>
</head>
<body>
	<header>
		<div id="header_inner">
			<h1>Bulletin board</h1>
			<p><i class="far fa-user-circle"></i> <c:out value="${loginUser.name}" />さん</p>
		</div>
		<nav class="admin">
			<ul>
				<li><a href="./"><i class="fas fa-home"></i>ホーム</a></li>
				<li><a href="post">新規投稿</a></li>
				<c:if test = "${loginUser.branchId == 1 && loginUser.divisionId == 1}">
					<li><a href="admin">ユーザー管理</a></li>
				</c:if>
				<c:if test = "${loginUser.branchId == 1 && loginUser.divisionId == 1}">
					<li><a href="signup">ユーザー登録</a></li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div id="wrapper">
		<h2>ユーザー管理画面</h2>
	</div>
		<div id="errorMessages">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>

		<table>
			<thead>
				<tr>
					<th>ログインID</th>
					<th>名称</th>
					<th>支店</th>
					<th>部署・役職</th>
					<th>復活/停止</th>
					<th>ステータス</th>
					<th>編集</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${users}" var="user">
					<tr>
						<td><c:out value="${user.loginId}" /></td>
						<td><c:out value="${user.name}" /></td>
						<td><c:out value="${user.branchName}" /></td>
						<td><c:out value="${user.divisionName}" /></td>
						<td><form class="table_form" action="alive" style="display:inline">
							<input type="hidden" value="${user.id}" name="editUserId">
							<input type="hidden" value="${user.isAlive}" name="isAlive">
							<c:choose>
								<c:when test="${user.id == loginUser.id }">
									<c:set var="result" value="ログイン中" />
								</c:when>
								<c:when test = "${user.isAlive == 1 }">
									<input type="submit" value="停止" onclick='return confirm("送信してよろしいですか？");'>
									<c:set var="result" value="稼働中" />
								</c:when>
								<c:otherwise>
									<input type="submit" value="復活" onclick='return confirm("送信してよろしいですか？");'>
									<c:set var="result" value="- 停止中 -" />
								</c:otherwise>
							</c:choose>
						</form></td>
						<td>${result}</td>
						<td><form class="table_form" action="edit" style="display:inline">
							<input type="hidden" value="${user.id}" name="editUserId">
							<input type="submit" value="編集">
						</form></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	<footer>Copyright(c) Matsunaga Shinya</footer>
</body>
</html>