<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/reset.css" rel="stylesheet" type="text/css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<title>ログイン画面</title>
</head>
<body>
	<header>
		<div id="header_inner"  class="clearfix">
			<h1>Bulletin board</h1>
		</div>
	</header>

	<div id="wrapper" class="clear">
		<h2>ログイン画面</h2>
		<div id="errorMessages">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>

		<div class="form">
			<form action="login" method="post">
				<div class="item">
					<label for="loginId">ログインID</label>
					<input type="text" id="loginId" name="loginId" value="${retainLoginId}">
				</div>
				<div class="item">
					<label for="pass">パスワード</label>
					<input type="password" name="password" id="pass">
				</div>
				<div class="submit_btn">
					<input type="submit" value="ログイン">
				</div>
			</form>
		</div>
	</div>

	<footer class="fixed">Copyright(c) Matsunaga Shinya</footer>
</body>
</html>