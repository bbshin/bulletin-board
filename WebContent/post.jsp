<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/reset.css" rel="stylesheet" type="text/css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<title>新規投稿画面</title>
</head>
<body>
	<header>
		<div id="header_inner">
			<h1>Bulletin board</h1>
			<p><i class="far fa-user-circle"></i> <c:out value="${loginUser.name}" />さん</p>
		</div>
		<nav>
			<ul>
				<li><a href="./"><i class="fas fa-home"></i>ホーム</a></li>
				<li><a href="post">新規投稿</a></li>
				<c:if test = "${loginUser.branchId == 1 && loginUser.divisionId == 1}">
					<li><a href="admin">ユーザー管理</a></li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div id="wrapper">
		<h2>新規投稿画面</h2>
		<div id="errorMessages">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
		<div id="post_form" class="form">
			<form action="post" method="post">
				<div class="item">
					<label for="category">カテゴリー</label>
					<input type="text" name="category" id="category" value="${retainPost.category}">
				</div>
				<div class="item">
					<label for="category">件名</label>
					<input type="text" name="subject" value="${retainPost.subject}">
				</div>
				<div class="item">
					<textarea name="text" cols="70" rows="5" wrap="hard" placeholder="投稿">${retainPost.text}</textarea>
				</div>
				<div class="submit_btn">
					<input type="submit" value="投稿">
				</div>
			</form>
		</div>
	</div>
	<footer>Copyright(c) Matsunaga Shinya</footer>
</body>
</html>