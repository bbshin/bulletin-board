<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@	taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/reset.css" rel="stylesheet" type="text/css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<title>ホーム画面</title>
</head>
<body>
	<header>
		<div id="header_inner">
			<h1>Bulletin board</h1>
			<p><i class="far fa-user-circle"></i> <c:out value="${loginUser.name}" />さん</p>
		</div>
		<nav>
			<ul>
				<li><a href="./"><i class="fas fa-home"></i>ホーム</a></li>
				<li><a href="post">新規投稿</a></li>
				<c:if test = "${loginUser.branchId == 1 && loginUser.divisionId == 1}">
					<li><a href="admin">ユーザー管理</a></li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div id="wrapper">
		<h2>ホーム画面</h2>
		<div id="errorMessages">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>

		<div id="search_form" class="form">
			<form action ="./" method="get">
				<p>検索</p>
				<div class="item">
					<label for="category">カテゴリー</label><input type="text" name="word" id="category" value="${word}">
				</div>
				<div class="item inline date">
					<input type="date" name="startDate" value="${startDate}"> ～ <input type="date" name="endDate" value="${endDate}">
				</div>
				<div class="submit inline">
					<input type="submit" value="検索">
				</div>
			</form>
		</div>

		<div id="display">
			<c:choose>
			<c:when test= "${not empty PostList}">

					<c:forEach items="${PostList}" var="post">
						<div class="posts">
							<div class="post">
								<dl>
									<dt class="subject"><span>件名：</span><c:out value= "${post.subject}" /></dt>
									<dt class="text"><p><c:out value= "${post.text}" /></p></dt>
									<dd class="category"><span>カテゴリ：</span><c:out value= "${post.category}" /></dd>
									<dd><c:out value= "${post.name}" /></dd>
									<dd><fmt:formatDate value= "${post.updatedDate}" pattern="yyyy/MM/dd HH:mm:ss" /></dd>
									<c:if test="${loginUser.id == post.userId }">
										<dd><form id="deletepost" action="deletepost" method="post"style="display:inline">
											<input type="hidden" name="postId" value="${post.id}">
											<input type="submit" value="投稿削除" onclick='return confirm("削除してよろしいですか？");'><br>
										</form></dd>
									</c:if>
								</dl>
							</div>



							<c:forEach items="${comments}" var="comment">
								<c:if test = "${post.id == comment.postId}">
									<div class="comment">
										<dl>
											<dt><p class="commentText"><c:out value="${comment.text}" /></p></dt>
											<dd><c:out value="${comment.name}" /></dd>
											<dd><fmt:formatDate value= "${comment.updatedDate}" pattern="yyyy/MM/dd HH:mm:ss" /></dd>
											<c:if test="${loginUser.id == comment.userId }">
												<dd><form id="deletecomment" action="deletecomment" method="post" style="display:inline">
													<input type="hidden" name="commentId" value="${comment.id}">
													<input type="submit" value="コメント削除" onclick='return confirm("削除してよろしいですか？");'>
												</form></dd>
											</c:if>
										</dl>
									</div>
								</c:if>
							</c:forEach>

							<div class="comment_form">
								<form action="comment" method="post">
									<div class="item inline">
										<textarea name="comment" wrap="hard"></textarea>
									</div>
									<input type="hidden" name="post_id" value="${post.id}">
									<div class="submit inline">
										<input type="submit" value="コメント">
									</div>
								</form>
							</div>
						</div>
					</c:forEach>
			</c:when>
			<c:otherwise>
				<div id="nothing">
					該当する投稿がありません
				</div>
			</c:otherwise>
			</c:choose>
		</div>
	</div>
	<footer>Copyright(c) Matsunaga Shinya</footer>
</body>
</html>