create database matsunaga_shinya;

create table users(
	id integer auto_increment primary key,
	login_id varchar(20) unique not null,
	name varchar(10) not null,
	password varchar(255) not null,
	branch_id integer not null,
	division_id integer not null,
	is_alive tinyint(1) not null,
	created_date timestamp not null default current_timestamp,
	updated_date timestamp not null default current_timestamp
);

create table branches(
	id integer auto_increment primary key,
	name varchar(20)  not null,
	created_date timestamp not null default current_timestamp,
	updated_date timestamp not null default current_timestamp
);

create table divisions(
	id integer auto_increment primary key,
	name varchar(20) not null,
	created_date timestamp not null default current_timestamp,
	updated_date timestamp not null default current_timestamp
);

create table posts(
	id integer auto_increment primary key,
	user_id integer not null,
	subject varchar(30) not null,
	text varchar(1000) not null,
	category varchar(10) not null,
	created_date timestamp not null default current_timestamp,
	updated_date timestamp not null default current_timestamp
);

create table comments(
	id integer auto_increment primary key,
	user_id integer not null,
	post_id integer not null,
	text varchar(500) not null,
	created_date timestamp not null default current_timestamp,
	updated_date timestamp not null default current_timestamp
);
